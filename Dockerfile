FROM azul/zulu-openjdk:8
ADD target/ocdemo-[0-9].[0-9].[0-9]-SNAPSHOT.jar ocdemo.jar
EXPOSE 8080
CMD java -jar ocdemo.jar
